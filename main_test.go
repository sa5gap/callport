// main_test
package main

import (
	"CallPort/model"
	"testing"
)

type testCase struct {
	pc  model.Portcall
	ds  model.Dues
	SUM float64
}

var tc = []testCase{
	{
		SUM: 8497,
		pc: model.Portcall{
			Vessel: model.Vessel{
				Name:      "Kazan",
				Loa:       114.4,
				Beam:      16.2,
				MDepth:    6.1,
				RGT:       2971,
				ReducedGT: 2673.9,
			},
		},
		ds: model.Dues{
			TONNAGE_DUES:                304,
			NAVIGATIONAL_DUES:           1166,
			LIGHT_DUES:                  173,
			PILOTAGE:                    155,
			ECOLOGICAL_SERVICES:         208,
			MOORING_UNMOORING:           450,
			TUGS:                        1696,
			FIREGUARD:                   207,
			DEPLOYMENT_REMOVAL_OF_BOOMS: 2000,
			CURRENCY_CONVERSION_FEES:    138,
			SUPERVISION:                 200,
			PORT_CLEARANCE:              250,
			COURIER_MAIL_SERVICES:       100,
			COMMUNICATION:               200,
			USE_OF_LICENSED_VEHICLE:     250,
			BANK_CHARGES:                250,
			AGENCY_FEE:                  750,
			Params: model.DuesParams{
				ROE: 47,
				PIL: 7,
			},
		},
	},
	{
		SUM: 10988,
		pc: model.Portcall{
			Vessel: model.Vessel{
				Name:      "Dafne",
				Loa:       139.9,
				Beam:      16.7,
				MDepth:    6.4,
				RGT:       4824,
				ReducedGT: 3654,
			},
		},
		ds: model.Dues{
			TONNAGE_DUES:                600,
			NAVIGATIONAL_DUES:           2305,
			LIGHT_DUES:                  342,
			PILOTAGE:                    306,
			ECOLOGICAL_SERVICES:         338,
			MOORING_UNMOORING:           680,
			TUGS:                        2243,
			FIREGUARD:                   274,
			DEPLOYMENT_REMOVAL_OF_BOOMS: 2000,
			CURRENCY_CONVERSION_FEES:    0,
			SUPERVISION:                 0,
			PORT_CLEARANCE:              250,
			COURIER_MAIL_SERVICES:       100,
			COMMUNICATION:               200,
			USE_OF_LICENSED_VEHICLE:     250,
			BANK_CHARGES:                100,
			AGENCY_FEE:                  1000,
			Params:                      model.DuesParams{},
		},
	},
}

func TestDues(t *testing.T) {
	for _, v := range tc {
		dues := model.NewDues(&v.pc, v.ds.Params)
		vn := v.pc.Vessel.Name
		if dues.SUM != v.SUM {
			t.Errorf("=== %s ===", vn)
			t.Error("* несоответствия: расчет <> образец")
			t.Errorf("%s - STRAIGHT TIME: %.0f <> %.0f", vn, dues.SUM, v.SUM)
			t.Error("* размерения:")
			t.Errorf("%s - Loa = %.2f\n", vn, v.pc.Vessel.Loa)
			t.Errorf("%s - Beam = %.2f\n", vn, v.pc.Vessel.Beam)
			t.Errorf("%s - MDepth = %.2f\n", vn, v.pc.Vessel.MDepth)
			t.Errorf("%s - RGT = %.2f\n", vn, v.pc.Vessel.RGT)
			t.Errorf("%s - ReducedGT = %.2f\n", vn, v.pc.Vessel.ReducedGT)
			t.Error("* коэффициенты:")
			t.Errorf("%s - BOTH = %.2f\n", vn, dues.Params.BOTH)
			t.Errorf("%s - TON = %.2f\n", vn, dues.Params.TON)
			t.Errorf("%s - LIG = %.2f\n", vn, dues.Params.LIG)
			t.Errorf("%s - NAV = %.2f\n", vn, dues.Params.NAV)
			t.Errorf("%s - ECO = %.2f\n", vn, dues.Params.ECO)
			t.Errorf("%s - PIL = %.2f\n", vn, dues.Params.PIL)
			t.Errorf("%s - FLE = %.3f\n", vn, dues.Params.FLE)
			t.Errorf("%s - FIR = %.4f\n", vn, dues.Params.FIR)
			t.Errorf("%s - ROE = %.2f\n", vn, dues.Params.ROE)
			t.Errorf("%s - MOO = %.2f\n", vn, dues.MOO)
			t.Errorf("%s - VOL = %.2f\n", vn, dues.VOL)
			t.Error("* несоответствия: расчет <> образец")
		}
		if dues.NAVIGATIONAL_DUES != v.ds.NAVIGATIONAL_DUES {
			t.Errorf("%s - NAVIGATIONAL_DUES: %.0f <> %.0f\n", vn,
				dues.NAVIGATIONAL_DUES, v.ds.NAVIGATIONAL_DUES)
		}
		if dues.LIGHT_DUES != v.ds.LIGHT_DUES {
			t.Errorf("%s - LIGHT_DUES: %.0f <> %.0f\n", vn,
				dues.LIGHT_DUES, v.ds.LIGHT_DUES)
		}
		if dues.PILOTAGE != v.ds.PILOTAGE {
			t.Errorf("%s - PILOTAGE: %.0f <> %.0f\n", vn,
				dues.PILOTAGE, v.ds.PILOTAGE)
		}
		if dues.ECOLOGICAL_SERVICES != v.ds.ECOLOGICAL_SERVICES {
			t.Errorf("%s - ECOLOGICAL_SERVICES: %.0f <> %.0f\n", vn,
				dues.ECOLOGICAL_SERVICES, v.ds.ECOLOGICAL_SERVICES)
		}
		if dues.MOORING_UNMOORING != v.ds.MOORING_UNMOORING {
			t.Errorf("%s - MOORING_UNMOORING: %.0f <> %.0f\n", vn,
				dues.MOORING_UNMOORING, v.ds.MOORING_UNMOORING)
		}
		if dues.TUGS != v.ds.TUGS {
			t.Errorf("%s - TUGS: %.0f <> %.0f\n", vn,
				dues.TUGS, v.ds.TUGS)
		}
		if dues.FIREGUARD != v.ds.FIREGUARD {
			t.Errorf("%s - FIREGUARD: %.0f <> %.0f\n", vn,
				dues.FIREGUARD, v.ds.FIREGUARD)
		}
		if dues.DEPLOYMENT_REMOVAL_OF_BOOMS != v.ds.DEPLOYMENT_REMOVAL_OF_BOOMS {
			t.Errorf("%s - DEPLOYMENT_REMOVAL_OF_BOOMS: %.0f <> %.0f\n", vn,
				dues.DEPLOYMENT_REMOVAL_OF_BOOMS, v.ds.DEPLOYMENT_REMOVAL_OF_BOOMS)
		}
		if dues.CURRENCY_CONVERSION_FEES != v.ds.CURRENCY_CONVERSION_FEES {
			t.Errorf("%s - CURRENCY_CONVERSION_FEES: 2%% от %.0f = %.0f\n", vn,
				dues.SUM_CCF, dues.CURRENCY_CONVERSION_FEES)
			t.Errorf("%s - CURRENCY_CONVERSION_FEES: %.0f <> %.0f\n", vn,
				dues.CURRENCY_CONVERSION_FEES, v.ds.CURRENCY_CONVERSION_FEES)
		}
		if dues.SUPERVISION != v.ds.SUPERVISION {
			t.Errorf("%s - SUPERVISION: %.0f <> %.0f\n", vn,
				dues.SUPERVISION, v.ds.SUPERVISION)
		}
		if dues.PORT_CLEARANCE != v.ds.PORT_CLEARANCE {
			t.Errorf("%s - PORT_CLEARANCE: %.0f <> %.0f\n", vn,
				dues.PORT_CLEARANCE, v.ds.PORT_CLEARANCE)
		}
		if dues.COURIER_MAIL_SERVICES != v.ds.COURIER_MAIL_SERVICES {
			t.Errorf("%s - COURIER_MAIL_SERVICES: %.0f <> %.0f\n", vn,
				dues.COURIER_MAIL_SERVICES, v.ds.COURIER_MAIL_SERVICES)
		}
		if dues.COMMUNICATION != v.ds.COMMUNICATION {
			t.Errorf("%s - COMMUNICATION: %.0f <> %.0f\n", vn,
				dues.COMMUNICATION, v.ds.COMMUNICATION)
		}
		if dues.USE_OF_LICENSED_VEHICLE != v.ds.USE_OF_LICENSED_VEHICLE {
			t.Errorf("%s - USE_OF_LICENSED_VEHICLE: %.0f <> %.0f\n", vn,
				dues.USE_OF_LICENSED_VEHICLE, v.ds.USE_OF_LICENSED_VEHICLE)
		}
		if dues.BANK_CHARGES != v.ds.BANK_CHARGES {
			t.Errorf("%s - BANK_CHARGES: %.0f <> %.0f\n", vn,
				dues.BANK_CHARGES, v.ds.BANK_CHARGES)
		}
		if dues.AGENCY_FEE != v.ds.AGENCY_FEE {
			t.Errorf("%s - AGENCY_FEE: %.0f <> %.0f\n", vn,
				dues.AGENCY_FEE, v.ds.AGENCY_FEE)
		}
	}
}
