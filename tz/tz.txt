
размерения судна :

длина, loa
ширина, beam
высота борта, moulded depth (m_depth)
объем судна, volume
Регистровый тоннаж гросс, RGT
Уменьшенный регистровый тоннаж гросс, reduced_gt (
(RGT - объем балластных танков)

стоимость cудозаход складывается из суммы портовых сборов

для расчета сбора обычно берется установленный коэффициент и
применяется в формуле содержащей размерения судна (обычно RGT или
объем судна)

объем : loa * beam * m_depth

применяются следующие портовые сборы :

КОРАБЕЛЬНЫй СБОР / TONNAGE DUES 
НАВИГАЦИОННЫЙ СБОР / NAVIGATIONAL DUES 
МАЯЧНЫЙ СБОР / LIGHT DUES 
ЛОЦМАНСКИЙ СБОР / PILOTAGE 
ЭКОЛОГИЧЕСКИЙ СБОР / ECOLOGICAL SERVICES 
ШВАРТОВКА/ОТШВАРТОВКА / MOORING/UNMOORING 
БУКСИРНЫЙ СБОР / TUGS 
ПОЖАРНЫЙ БУКСИР / FIREGUARD 
ПОСТАНОВКА/СНЯТИЕ БОНОВ / DEPLOYMENT AND REMOVAL OF BOOMS

Коэффициенты :

BOTH = 2; - швартовка и отшвартовка
TON = 2.67; - тоннажный
LIG = 1.52; - маячный
NAV = 10.25; - навигационный
ECO = .14; - экологический

швартовка/отшвартовка зависит от RGT :

MOO = Case (

Vessels::rgt  ≤ 1335    ; 200;
Vessels::rgt  ≤ 6270    ; 360;
Vessels::rgt  ≤ 18800   ; 530;
Vessels::rgt  ≤ 29340   ; 770;
Vessels::rgt  ≤ 40000   ; 880;
Vessels::rgt  ≤ 53350   ; 1010;
Vessels::rgt  ≤ 62700   ; 1300;
Vessels::rgt  ≤ 80000   ; 1470;
Vessels::rgt  ≤ 200000  ; 1730;
Vessels::rgt  > 200000  ; 0 );


лоцманский коэф : 6
PIL = 6

буксиры :
FLE = .075;

пожарный буксир :
FIR = .0183;

курс доллара
USD = 60.0

портовые сборы расчитываются по следующим формулам :

"TONNAGE DUES"; ((TON * Vessels::reduced_gt) / USD) * BOTH;
"NAVIGATIONAL DUES"; ((NAV * Vessels::reduced_gt) / USD) * BOTH;
"LIGHT DUES"; ((LIG * Vessels::reduced_gt) / USD) * BOTH;
"PILOTAGE"; ((.13 * Vessels::reduced_gt * PIL)  / USD) * BOTH + ((.45 * Vessels::reduced_gt) / USD ) * BOTH;
"ECOLOGICAL SERVICES"; (ECO * Vessels::rgt) / 2;
"MOORING/UNMOORING"; MOO * 1.25 * 2; /* * BOTH * USD; */
"TUGS"; FLE * Vessels::loa * Vessels::beam * Vessels::m_depth * BOTH ;
"FIREGUARD"; FIR * Vessels::loa * Vessels::beam * Vessels::m_depth;
"WATCHMAN SERVICE - OBLIGATORY ITEM"; 232 ;
"USE OF TERMINAL'S BUS - OBLIGATORY ITEM"; 413 ;
"USE OF TERMINAL'S GANGWAY - OBLIGATORY ITEM"; 1416 ;
"DEPLOYMENT / REMOVAL OF BOOMS"; 2000 ;


потом это все суммируется и получается стоимость судозахода.

то есть на входе размерения судна - на выходе сумма портовых сборов


таблицы в бд :

Portcalls (Судозаходы)
Vessels (Суда)
Сборы (Sbory)

кроме сборов есть еще обязательные расходы (Sundries) 
они обычно фиксированные :

"CURRENCY CONVERSION FEES - 2%";  _PDA_SUM * .02 ;
"SUPERVISION"; 200 ;
"PORT CLEARANCE - OBLIGATORY ITEM"; 250 ;
"COURIER MAIL SERVICES"; 100 ;
"COMMUNICATION"; 200 ;
"USE OF CAR FOR SHIP'S BUSINESS (ROUND-THE-CLOCK)"; 250 ;
"USE OF LICENSED VEHICLE FOR TRANSPORTATION OF PORT AUTHORITIES"; 250 ;
"BANK CHARGES"; 250 ;
"AGENCY FEE"; 250

Этот расчет называется PDA - проформа дисбурсментского счета

дальше можно будет добавить таблицы портов и причалов (набор сборов разнится от причала к причалу, но это потом)

после ухода судна из порта расчитывается FDA - финальный счет (по факту, суммы из квитанций выданных портом)

то есть будет на одной странице две таблички, PDA и FDA


