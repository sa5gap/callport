// main
package main

import (
	"CallPort/model"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kr/pretty"
)

const dbFile = "./db.sqlite"

var db *gorm.DB

func init() {

}

func main() {
	var (
		vessels   []model.Vessel
		portcalls []model.Portcall
		dues      *model.Dues
		output    string
	)

	output = "CallPort\n"

	var err1 error
	db, err1 = gorm.Open("sqlite3", dbFile)
	if err1 != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	//db.LogMode(true)

	//	запрос всех судов
	output += fmt.Sprintf("\n=== СУДА ===\n\n")
	db.Find(&vessels)
	for i := range vessels {
		// orm assoc: Has Many
		db.Model(&vessels[i]).Related(&vessels[i].Portcalls)
		db.Model(&vessels[i]).Related(&vessels[i].Owner)
		output += fmt.Sprintln("--- судно:", vessels[i].Name)
		output += fmt.Sprintf("%# v\n", pretty.Formatter(vessels[i]))
	}

	//	запрос всех заходов
	output += fmt.Sprintf("\n=== ЗАХОДЫ ===\n\n")
	db.Find(&portcalls)
	for i := range portcalls {
		// orm assoc: Belongs To
		db.Model(&portcalls[i]).Related(&portcalls[i].Vessel)
		db.Model(&portcalls[i].Vessel).Related(&portcalls[i].Vessel.Owner)
		db.Model(&portcalls[i]).Related(&portcalls[i].Berth)
		db.Model(&portcalls[i].Berth).Related(&portcalls[i].Berth.Port)
		output += fmt.Sprintln("--- заход:", portcalls[i].Berth.Port.Name, "/ причал #", portcalls[i].Berth.Name)
		output += fmt.Sprintf("%# v\n", pretty.Formatter(portcalls[i]))
		dues = model.NewDues(&portcalls[i], model.DuesParams{})
		output += fmt.Sprintln("--- сборы:")
		output += fmt.Sprintf("%# v\n", pretty.Formatter(*dues))
	}

	//	запрос всех портов
	output += fmt.Sprintf("\n=== ПОРТЫ ===\n\n")
	ports := []model.Port{}
	db.Find(&ports)
	for i := range ports {
		// orm assoc: Has Many
		db.Model(&ports[i]).Related(&ports[i].Berths)
		output += fmt.Sprintln("--- порт:", ports[i].Name)
		output += fmt.Sprintf("%# v\n", pretty.Formatter(ports[i]))
	}

	// router
	//	r := chi.NewRouter()
	//	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
	//		w.Write([]byte(output))
	//	})
	//	http.ListenAndServe(":3000", r)

	// классический http сервер

	// html шаблоны
	var templates = template.Must(template.ParseFiles("./view/test.html", "./view/vessels.html"))

	http.HandleFunc("/portcall/", viewHandler)
	http.HandleFunc("/vessels/", viewVessels)
	http.HandleFunc("/owners/", viewOwners)

	// Каждый запрос вызывает обработчик
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			log.Print(err)
		}
		//templates = nil
		//fmt.Fprintf(w, "%v\n\n%# v", output, pretty.Formatter(*r))
		err := templates.ExecuteTemplate(w, "test.html", r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	log.Fatal(http.ListenAndServe(":3000", nil))

}

// Обработчик возвращает компонент пути из URL запроса,
func viewOwners(w http.ResponseWriter, r *http.Request) {
	var d []model.Owner
	db.Find(&d)
	t, _ := template.ParseFiles("view/owners.html")
	t.Execute(w, d)
}

// Обработчик возвращает компонент пути из URL запроса,
func viewVessels(w http.ResponseWriter, r *http.Request) {
	var d []model.Vessel
	db.Find(&d)
	for i := range d {
		db.Model(&d[i]).Related(&d[i].Owner)
	}
	t, _ := template.ParseFiles("view/vessels.html")
	t.Execute(w, d)
}

// Обработчик возвращает компонент пути из URL запроса,
func viewHandler(w http.ResponseWriter, r *http.Request) {
	var pc model.Portcall
	var id int

	if _, err := fmt.Sscanf(r.URL.Path, "/portcall/%d", &id); err == nil {
		if db.First(&pc, id).RecordNotFound() {
			errMsg := fmt.Sprintf("Запись `%d` не найдена", id)
			http.Error(w, errMsg, http.StatusInternalServerError)
			return
		}
		db.Model(&pc).Related(&pc.Vessel)
		db.Model(&pc.Vessel).Related(&pc.Vessel.Owner)
		db.Model(&pc).Related(&pc.Berth)
		db.Model(&pc.Berth).Related(&pc.Berth.Port)
		pc.Dues = *model.NewDues(&pc, model.DuesParams{})
		t, _ := template.ParseFiles("view/test.html")
		t.Execute(w, pc)
		//		fmt.Fprintf(w, "<pre>%# v", pretty.Formatter(pc))
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
