// Package model / berth
package model

// Berth -- структура описания причалов
type Berth struct {
	ID     int    `sql:"AUTO_INCREMENT" gorm:"primary_key"`
	Name   string // название
	PortID int    // порт
	Port   Port
}
