// vessels
package model

// размерения судна
// TODO: перейти на новый стандарт ID
// FIXME: сделать работающий тест
type Vessel struct {
	ID        int        `sql:"AUTO_INCREMENT" gorm:"primary_key"` // если будем сливать БД, нужен строковый id
	Name      string     // название
	Loa       float64    // длина, loa
	Beam      float64    // ширина, beam
	MDepth    float64    // высота борта, moulded depth
	RGT       float64    // Регистровый тоннаж гросс, RGT
	ReducedGT float64    // Уменьшенный регистровый тоннаж гросс, reduced_gt
	Portcalls []Portcall // Судозаходы
	OwnerID   int        // идентификатор Владельца судна
	Owner     Owner      // Владелец
}
