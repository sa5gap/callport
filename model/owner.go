// owner
package model

type Owner struct {
	Id   int    `sql:"AUTO_INCREMENT" gorm:"primary_key"`
	Name string // название
}
