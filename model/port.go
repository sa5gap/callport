// port
package model

type Port struct {
	Id     int     `sql:"AUTO_INCREMENT" gorm:"primary_key"`
	Name   string  // название
	Berths []Berth // все причалы порта
}
