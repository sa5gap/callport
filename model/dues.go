// dues
package model

import (
	"math"

	"github.com/imdario/mergo"
)

// Параметры / коэффициенты
type DuesParams struct {
	BOTH float64 // - швартовка и отшвартовка
	TON  float64 // - тоннажный
	LIG  float64 // - маячный
	NAV  float64 // - навигационный
	ECO  float64 // - экологический
	PIL  float64 // лоцманский коэф
	FLE  float64 // буксиры
	FIR  float64 // пожарный буксир
	ROE  float64 // курс доллара
}

// Взносы
type Dues struct {
	// нефиксированные
	TONNAGE_DUES                float64
	NAVIGATIONAL_DUES           float64
	LIGHT_DUES                  float64
	PILOTAGE                    float64
	ECOLOGICAL_SERVICES         float64
	MOORING_UNMOORING           float64
	TUGS                        float64
	FIREGUARD                   float64
	WATCHMAN_SERVICE            float64
	USE_OF_TERMINALS_BUS        float64
	USE_OF_TERMINALS_GANGWAY    float64
	DEPLOYMENT_REMOVAL_OF_BOOMS float64

	// фиксированные
	CURRENCY_CONVERSION_FEES float64
	SUPERVISION              float64
	PORT_CLEARANCE           float64
	COURIER_MAIL_SERVICES    float64
	COMMUNICATION            float64
	USE_OF_CAR               float64
	USE_OF_LICENSED_VEHICLE  float64
	BANK_CHARGES             float64
	AGENCY_FEE               float64

	// сумма
	SUM     float64
	SUM_CCF float64

	// параметры
	Params DuesParams

	// промежуточные коэффициенты
	MOO float64 // швартовка/отшвартовка
	VOL float64 // объем
}

var defaultDuesParam DuesParams = DuesParams{
	BOTH: 2,
	TON:  2.67,
	LIG:  1.52,
	NAV:  10.25,
	ECO:  .14,
	PIL:  6,
	FLE:  .075,
	FIR:  .0183,
	ROE:  60.0,
}

// конструктор модели
func NewDues(pc *Portcall, p DuesParams) *Dues {
	v := pc.Vessel
	d := Dues{}

	// сведение параметров
	mergo.Merge(&p, defaultDuesParam)
	d.Params = p

	// швартовка/отшвартовка зависит от RGT :
	//	d.MOO = 0
	//	switch {
	//	case v.RGT <= 1335:
	//		d.MOO = 200
	//	case v.RGT <= 6270:
	//		d.MOO = 360
	//	case v.RGT <= 18800:
	//		d.MOO = 530
	//	case v.RGT <= 29340:
	//		d.MOO = 770
	//	case v.RGT <= 40000:
	//		d.MOO = 880
	//	case v.RGT <= 53350:
	//		d.MOO = 1010
	//	case v.RGT <= 62700:
	//		d.MOO = 1300
	//	case v.RGT <= 80000:
	//		d.MOO = 1470
	//	case v.RGT <= 200000:
	//		d.MOO = 1730
	//	}

	switch {
	case v.RGT <= 270:
		d.MOO = 28
	case v.RGT <= 545:
		d.MOO = 45
	case v.RGT <= 820:
		d.MOO = 80
	case v.RGT <= 1780:
		d.MOO = 157
	case v.RGT <= 3550:
		d.MOO = 225
	case v.RGT <= 7240:
		d.MOO = 340
	case v.RGT <= 13660:
		d.MOO = 510
	case v.RGT <= 20500:
		d.MOO = 620
	case v.RGT <= 27300:
		d.MOO = 730
	case v.RGT <= 34150:
		d.MOO = 840
	case v.RGT <= 100000:
		d.MOO = 960
	case v.RGT > 100000:
		d.MOO = 0
	}

	// объем
	d.VOL = v.Loa * v.Beam * v.MDepth

	// основной рассчет
	d.TONNAGE_DUES = Round(((p.TON * v.ReducedGT) / p.ROE) * p.BOTH)
	d.NAVIGATIONAL_DUES = Round(((p.NAV * v.ReducedGT) / p.ROE) * p.BOTH)
	d.LIGHT_DUES = Round(((p.LIG * v.ReducedGT) / p.ROE) * p.BOTH)
	d.PILOTAGE = Round(((.13*v.ReducedGT*p.PIL)/p.ROE)*p.BOTH + ((.45*v.ReducedGT)/p.ROE)*p.BOTH)
	d.ECOLOGICAL_SERVICES = Round((p.ECO * v.RGT) / 2)
	d.MOORING_UNMOORING = Round(d.MOO * p.BOTH) // BOTH * ROE
	d.TUGS = Round(p.FLE * d.VOL * p.BOTH)
	d.FIREGUARD = Round(p.FIR * d.VOL)
	d.WATCHMAN_SERVICE = 232          // OBLIGATORY ITEM
	d.USE_OF_TERMINALS_BUS = 413      // OBLIGATORY ITEM
	d.USE_OF_TERMINALS_GANGWAY = 1416 // OBLIGATORY ITEM
	d.DEPLOYMENT_REMOVAL_OF_BOOMS = 2000

	d.SUPERVISION = 200
	d.PORT_CLEARANCE = 250
	d.COURIER_MAIL_SERVICES = 100
	d.COMMUNICATION = 200
	d.USE_OF_CAR = 250
	d.USE_OF_LICENSED_VEHICLE = 250
	d.BANK_CHARGES = 250
	d.AGENCY_FEE = 250

	// рассчет CURRENCY_CONVERSION_FEES
	d.SUM_CCF = d.FIREGUARD
	d.SUM_CCF += d.DEPLOYMENT_REMOVAL_OF_BOOMS
	d.SUM_CCF += d.TUGS
	d.SUM_CCF += d.MOORING_UNMOORING
	d.SUM_CCF += d.ECOLOGICAL_SERVICES
	d.SUM_CCF += d.PILOTAGE
	d.SUM_CCF += d.LIGHT_DUES
	d.SUM_CCF += d.NAVIGATIONAL_DUES
	d.SUM_CCF += d.TONNAGE_DUES
	d.CURRENCY_CONVERSION_FEES = Round(d.SUM_CCF * .02)

	// портовые взносы
	d.SUM = d.TONNAGE_DUES
	d.SUM += d.NAVIGATIONAL_DUES
	d.SUM += d.LIGHT_DUES
	d.SUM += d.PILOTAGE
	d.SUM += d.ECOLOGICAL_SERVICES
	d.SUM += d.MOORING_UNMOORING
	d.SUM += d.TUGS
	d.SUM += d.FIREGUARD
	d.SUM += d.WATCHMAN_SERVICE
	d.SUM += d.USE_OF_TERMINALS_BUS
	d.SUM += d.USE_OF_TERMINALS_GANGWAY
	d.SUM += d.DEPLOYMENT_REMOVAL_OF_BOOMS

	// фиксированные взносы
	d.SUM += d.SUPERVISION
	d.SUM += d.PORT_CLEARANCE
	d.SUM += d.COURIER_MAIL_SERVICES
	d.SUM += d.COMMUNICATION
	d.SUM += d.USE_OF_CAR
	d.SUM += d.USE_OF_LICENSED_VEHICLE
	d.SUM += d.BANK_CHARGES
	d.SUM += d.AGENCY_FEE
	d.SUM += d.CURRENCY_CONVERSION_FEES

	return &d
}

func Round(f float64) float64 {
	return math.Floor(f + .5)
}
