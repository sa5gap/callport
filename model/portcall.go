// Portcalls
package model

// Судозаходы
// TODO: перейти на новый стандарт ID
type Portcall struct {
	Id   int  `sql:"AUTO_INCREMENT" gorm:"primary_key"` // Id
	Time uint // время захода
	//	PortId   int // порт
	//	Port     Port
	BerthId  int // причал - содержит в себе id порта
	Berth    Berth
	VesselId int    // идентификатор судна
	Vessel   Vessel // судно, бывшее в порту
	Dues     Dues   // Взносы
}
